﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppWithList.Models;

namespace WebAppWithList.Controllers
{
    public static class Globals
    {
        public static List<Employee> EmpList = new List<Employee>();
    }
    public class HomeController : Controller
    {  
        public ActionResult Index()
        {
            //TempData["list"] = EmpList;
            return View();
        }

        public ActionResult AddEmployee()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddEmployee(Employee emp)
        {
            //TempData["list"] = EmpList;
//            List<EmpModel> NewList = new List<EmpModel>();
            try
            {
                if (ModelState.IsValid)
                {
                    var imgPath = "";
                    var filePath = "";
                    foreach (HttpPostedFileBase file in emp.Files)
                    {
                        //Checking file is available to save.  
                        if (file != null && file.ContentType.Contains("image"))
                        {
                            var inputFileName = Path.GetFileName(file.FileName);
                            var path = Server.MapPath("~/Content/ImageEmp");
                            imgPath = Path.Combine(path, inputFileName);
                            //Save file to server folder  
                            file.SaveAs(imgPath);
                        }
                        else if (file != null)
                        {
                            var inputFileName = Path.GetFileName(file.FileName);
                            var path = Server.MapPath("~/Content/FileEmp");
                            filePath = Path.Combine(path, inputFileName);
                            //Save file to server folder  
                            file.SaveAs(filePath);

                        }

                    }
                    //assigning file uploaded status to ViewBag for showing message to user.  
                    ViewBag.UploadStatus = emp.Files.Count().ToString() + " files uploaded successfully.";
                    emp.ImgPath = imgPath;
                    emp.FilePath = filePath;
                    Globals.EmpList.Add(emp);
                }
                
            }
            
            catch(Exception e)
            {
                ViewBag.List = "List Empty"+e;
            }
            return RedirectToAction("ShowEmployee");
        }
        public ActionResult ShowEmployee()
        {
            ViewBag.List = Globals.EmpList;
            return View();
        }

        public ActionResult DeleteEmployee()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DeleteEmployee(int id)
        {
            var obj = Globals.EmpList.Where(s => s.Id == id).FirstOrDefault();
            if (obj!=null)
            {
                Globals.EmpList.Remove(obj);
                ViewBag.Message = "Item Deleted";

            }
            else
            {
                ViewBag.Message = "Item Not Found";
            }
            return View();
        }
        public ActionResult AskForEdit()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AskForEdit(int id)
        {
            TempData["Id"] = id;
            return RedirectToAction("EditDetails");
        }

        public ActionResult EditDetails()
        {
            return View();
        }
        [HttpPost]
        public ActionResult EditDetails(Employee emp)
        {
            var id = TempData["Id"].ToString();
            var item = Globals.EmpList.Where(s => s.Id == int.Parse(id)).FirstOrDefault();
            
                if (emp.EmpName != null)
                {
                    item.EmpName = emp.EmpName;
                }
                if (emp.EmpCode != null)
                {
                    item.EmpCode = emp.EmpCode;
                }
                if (emp.PhoneNumber.ToString() != null)
                {
                    item.PhoneNumber = emp.PhoneNumber;
                }
                if (emp.Salary.ToString() != null)
                {
                    item.Salary = emp.Salary;
                }

            return RedirectToAction("ShowEmployee");
        }

    }
}