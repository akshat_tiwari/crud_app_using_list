﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppWithList.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public long PhoneNumber { get; set; }
        public int Salary { get; set; }

        public HttpPostedFileBase[] Files { get; set; }
        public string FilePath { get; set; }
        public string ImgPath { get; set; }
    }

    
}