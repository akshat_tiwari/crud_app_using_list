﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebAppWithList.Models
{
    public class Employee
    {
        [Required(ErrorMessage ="Please fill this field")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please fill this field")]
        public string EmpCode { get; set; }

        [Required(ErrorMessage = "Please fill this field")]
        public string EmpName { get; set; }

        [Required(ErrorMessage = "Please fill this field")]
        public long PhoneNumber { get; set; }

        [Required(ErrorMessage = "Please fill this field")]
        public int Salary { get; set; }

        [Required(ErrorMessage = "Please fill this field")]
        public HttpPostedFileBase[] Files { get; set; }
        public string FilePath { get; set; }
        public string ImgPath { get; set; }
    }

    
}